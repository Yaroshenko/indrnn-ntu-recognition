run:
	docker run --runtime=nvidia -it -v ~/workspace/indrnn/:/workspace pytorch/pytorch:1.0.1-cuda10.0-cudnn7-devel

attach:
	docker exec -it $(shell sh -c "docker ps| grep pytorch/pytorch" | awk '{print $$1}') /bin/bash
